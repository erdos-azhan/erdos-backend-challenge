# Django Challenge

## Requirements
- python3
- [pipenv](https://pypi.org/project/pipenv/)
  - Can be installed with `pip install pipenv`
- [docker](https://docs.docker.com/engine/install/) 
  - Ensure docker compose is installed alongside docker
- [vscode](https://code.visualstudio.com/) (recommended, not required)
## Setup

1. Clone the repository
2. Bring the containers up with `docker compose up -d`
3. pgAdmin is running on [http://localhost:9090/](http://localhost:9090/); nothing needs to be configured
4. mailhog is running on [http://localhost:8025/](http://localhost:8025/); nothing needs to be configured
5. Run `pipenv install --dev` in project root
> Running `pipenv shell` in the project directory will allow you to go into the python virtual environment
6. Configure vscode to use the virutal environment
   1. Open the project folder with vscode
   2. Open a project python file
   3. Click python version bottom right of vscode
   4. Select the pipenv virutal environment with the name of the project
7. Run migration `python manage.py migrate`
8. VSCode debug configurations have already been created, so it can be run under the debug tab
9. Once the backend is running, visit [http://localhost:8000/api/schema/swagger-ui/](http://localhost:8000/api/schema/swagger-ui/) to view your endpoints
> **Important Note**: Custom endpoints do not need to be shown in the swagger ui/ openapi spec. Also some of your endpoints may not reflect in the swagger ui correctly. **This is fine.**


## Technical Challenge

Technical challenges require you modify the actual files.

Challenges are ranked in terms of stars from 1 star being the easier to 3 star being the hardest.

You should have been given instruction on which difficulty you are expected to complete up to.

### Difficulty: ⭐ 
- Create API endpoint `GET /post/recent/` that returns only posts that were created less than 2 days ago
- Create endpoint or Update the `PostModelViewSet` to return the `Post` ordered from most recently created to least recently.
### Difficulty: ⭐⭐
- Update the `PostModelViewSet` to have an extra field called `comments_count` on the returned `Post` objects. `comments_count` counts should be a number that says how many comments belong to that `Post`
  - Should be returned on `GET /post/` and `GET /post/{id}/` endpoints
    ```
    {
      "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "created": "2022-11-19T05:48:03.570Z",
      "modified": "2022-11-19T05:48:03.570Z",
      "title": "Avengers was great",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
      "comments_count": 3
    }
    ``` 

### Difficulty: ⭐⭐⭐
- Create a API Endpoint `GET /post/avg-comments-by-year-month/` that returns the average number of comments per month, independent of year. The output is flexible due to difficulty of this question. You can choose your own output.
  
  Example output:
  ```
  {
    '2022': {
      'JANUARY': 3,
      'FEBUARY': 5,
      ...
    }
  }
  ```
  or

  ```
  {
    '2022': {
      1: 3,
      2: 5,
      ...
    }
  }
  ```

  or

  ```
  {
    '2022_1': 3,
    '2022_2': 5,
  }
  ```
   > **The only restriction is you  are not allowed to use for loops for the calculation of the average comments per month and must be either SQL or Django Queries**. For loops can be used for the generation of the response


### Theoretical Challenge

The theoretical challenge only need a written response. A short summary will suffice. Include as much technical details as possible.

1. If we wanted to cache the results from technical challenge, specifically the `GET /post/recent/`, how would we cache this? How would we ensure that the cache is not stale and is returning the correct result?
2. If `READ` operations from the database are slow, what could we do to alleviate this problem without implementing caching?
