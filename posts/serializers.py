from rest_framework.serializers import ModelSerializer
from .models import Post, Comment

class PostSerializer(ModelSerializer):
    class Meta:
        model = Post
        read_only_fields = ("created", "modified")
        fields = "__all__"

class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        read_only_fields = ("created", "modified")
        fields = "__all__"