from rest_framework.routers import DefaultRouter
from .views import PostModelViewSet, CommentModelViewSet

router = DefaultRouter()
router.register('post', PostModelViewSet)
router.register('comment', CommentModelViewSet)

urlpatterns = router.urls
