from rest_framework.viewsets import ModelViewSet

from .models import Comment, Post
from .serializers import CommentSerializer, PostSerializer


class PostModelViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class CommentModelViewSet(ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer