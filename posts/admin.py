from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Comment, Post


@admin.register(Post)
class PostAdmin(ModelAdmin):
    pass


@admin.register(Comment)
class CommentAdmin(ModelAdmin):
    pass

