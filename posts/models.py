import uuid

from django.db.models import CASCADE, ForeignKey
from django.db.models.fields import TextField, UUIDField
from django_extensions.db.models import TimeStampedModel


class Post(TimeStampedModel):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = TextField()
    description = TextField()

    def __str__(self):
        return self.title
    


class Comment(TimeStampedModel):
    id = UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    body = TextField()
    post = ForeignKey(Post, on_delete=CASCADE)
